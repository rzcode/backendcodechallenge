<?php

namespace App\Repository;

use App\Domain\Commits\Services\VcsInterface;
use App\Entity\Commit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;

/**
 * @method Commit|null find($id, $lockMode = null, $lockVersion = null)
 * @method Commit|null findOneBy(array $criteria, array $orderBy = null)
 * @method Commit[]    findAll()
 * @method Commit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommitRepository extends ServiceEntityRepository
{
  use LoggerAwareTrait;

  /**
   * @var VcsInterface
   */
  private VcsInterface $vcs;

  public function __construct(ManagerRegistry $registry, LoggerInterface $logger, VcsInterface $vcs)
  {
    parent::__construct($registry, Commit::class);
    $this->logger = $logger;
    $this->vcs = $vcs;
  }

  /**
   * @return bool
   */
  public function refresh(): bool
  {
    try {
      $em = $this->getEntityManager();
      $em->getConnection()->delete('commit', ['1' => '1']);
      $newCommits = $this->vcs->fetchCommits(25, 'nodejs', 'node');
      $newCommits->map(
        function ($entity) use ($em) {
          $em->persist($entity);
        }
      );
      $em->flush();
      $this->logger->info("Refreshed !!!");
      return true;
    } catch (\Throwable $e) {
      $this->logger->error(\sprintf("Can't clear commits table %s", $e->getMessage()));
      return false;
    }
  }
}
