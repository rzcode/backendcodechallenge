<?php

namespace App\Controller;

use App\Repository\CommitRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/index", name="index")
     */
    public function index(CommitRepository $commitRepository): Response
    {
        $commits = $commitRepository->findBy([],[],25);
        return $this->render('index/index.html.twig', [
            'controller_name' => 'IndexController',
            'commits'=> $commits,
        ]);
    }

  /**
   * @Route("/refresh", name="refresh")
   * @param VcsInterface $vcs
   * @return Response
   */
  public function refresh(CommitRepository $commitRepository): Response
  {
    return $this->redirectToRoute('index', ['refreshed' => $commitRepository->refresh()]);
  }

}
