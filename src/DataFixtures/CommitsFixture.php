<?php

namespace App\DataFixtures;

use App\Domain\Commits\Services\VcsInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;

class CommitsFixture extends Fixture
{
  use LoggerAwareTrait;
  /**
   * @var VcsInterface
   */
  private $vcs;

  /**
   * Commits constructor.
   * @param VcsInterface $vcs
   * @param LoggerInterface $logger
   */
  public function __construct(VcsInterface $vcs, LoggerInterface $logger)
  {
    $this->vcs = $vcs;
    $this->logger = $logger;
  }

  public function load(ObjectManager $manager)
  {
    try {
      $lastCommitsCollection = $this->vcs->fetchCommits(25, 'nodejs', 'node');
      foreach ($lastCommitsCollection as $commit) {
        $manager->persist($commit);
      }
      $manager->flush();
    } catch(\Throwable $e) {
      $this->logger->error(\sprintf("Error while fetching new most recent commits", $e->getMessage()), $e->getTrace());
    }
  }
}
