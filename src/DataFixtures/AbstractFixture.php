<?php
namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

abstract class AbstractFixture extends Fixture
{
  /** @var ObjectManager */
  private $manager;

  abstract protected function loadData(ObjectManager $em);

  /**
   * @param ObjectManager $manager
   */
  public function load(ObjectManager $manager)  {
    $this->manager = $manager;
    $this->loadData($manager);
  }
}
