<?php

namespace App\Domain\Commits\Services;

use Doctrine\Common\Collections\ArrayCollection;

interface VcsInterface
{
  /**
   * @return bool
   */
  function Auth(): bool;

  /**
   * @param int $max
   * @param string $owner
   * @param string $repoName
   * @return ArrayCollection
   */
  function fetchCommits(int $max, string $owner, string $repoName): ArrayCollection;
}
