<?php

namespace App\Domain\Commits\Services;

use App\Entity\Commit;
use Doctrine\Common\Collections\ArrayCollection;
use GuzzleHttp\Client;

class GitHubService implements VcsInterface
{
  private Client $client;

  private array $headers;

  public function __construct(array $config = [])
  {
    $this->client = new Client(
      [
        'base_uri' => $config['baseUri'],
        'timeout' => 0,
        'allow_redirects' => true,
      ]
    );

    $this->headers = [
      'Accept' => 'application/vnd.github.v3+json'
    ];
  }

  /**
   * @inheritDoc
   */
  function Auth(): bool
  {
    // currently there is no need for auth
    return true;
  }

  /**
   * @inheritDoc
   */
  function fetchCommits(int $max, string $owner, string $repoName): ArrayCollection
  {
    $request = $this->client->get(
      \sprintf("repos/%s/%s/%s?per_page=%s", $owner, $repoName, "commits", $max),
      $this->headers
    );

    // todo move to a transformer
    $commits = json_decode($request->getBody()->getContents());
    $return = new ArrayCollection();
    foreach ($commits as $commitData) {
      $commit = new Commit();
      $commit->setAuthor($commitData->commit->author->name);
      $commit->setComment($commitData->commit->message);
      $commit->setCommitHash($commitData->sha);
      $commit->setCreatedAt(new \DateTimeImmutable($commitData->commit->committer->date));
      $return->add($commit);
    }
    return $return;
  }
}
