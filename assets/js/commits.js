import Commits from "./pages/commits/Commits";

document.addEventListener("DOMContentLoaded", function(event) {
  const commits = new Commits();
  commits.attachListeners();
});
