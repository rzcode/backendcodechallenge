class Commits {
  constructor() {
    this.searchInput = document.getElementById('findAuthor');
    this.noResults = document.getElementById('no-results');
  }
  attachListeners() {
    this.searchInput.addEventListener("keyup", (ev) => {
      this.findAuthor(this.sanitizeString(ev.currentTarget.value))
    });
  }

  findAuthor(author) {
    console.log(author);
    if(author.length === 0) {
      this.showAll();
      return;
    }

    this.hideAll();
    let matches = document.querySelectorAll(`div[data-id*=${author}]`);

    // show only the matches
    if(matches) {
      this.noResults.classList.add('d-none')
      matches.forEach((row)=> {
        row.classList.remove('d-none')
      })
    } else {
      this.noResults.classList.remove('d-none')
    }
  }

  hideAll() {
    document.querySelectorAll(`.dataRow`).forEach((row)=>{
      row.classList.add('d-none');
    });
  }

  showAll() {
    document.querySelectorAll(`.dataRow`).forEach((row)=>{
      row.classList.remove('d-none');
    });
  }

  sanitizeString(str){

    str = str.toLowerCase().replace(" ","");
    return str.trim();
  }
}

export default Commits;
