## Backend Coding Challenge

In order to be considered for the Backend position, you must complete the following task.

*Note: This task should take no longer than 2-3 hours at the most.*

### Prerequisites

- Experience with [PHP](http://www.php.net) Symfony framework
- Understanding of CSS frameworks and grid systems (Bootstrap, Pure, etc.)
- Database knowledge (MySQL, MongoDB, Postgres, etc.)

## Task

1. Fork this repository
2. Create a *source* folder to contain your code.
3. In the *source* directory, please create a PHP web application using a Symfony framework
4. Your application should accomplish the following:

* Connect to the [Github API](http://developer.github.com/)

* Find the [nodejs/node](https://github.com/nodejs/node) repository

* Find the 25 most recent commits

* Create a model and store the 25 most recent commits in the database. Make sure to avoid any duplicates.

* Create a basic template and utilize a CSS framework (Bootstrap, Pure, etc.)

* Create a route and view which displays the recent commits by author from the database.

* If the commit hash ends in a number, color that row light blue (#E6F1F6).

* Keep your solution flexible enought to be able to later on provide support for the bitbucket-api/gitlab-api etc.


### Once Complete
1. Create a SETUP.md in the base directory with setup instructions.
2. Please let us know that you have completed the challege and grant access to the repo to jobs@circunomics.com

## Key Points We Are Looking For
* Demonstration of core MVC patterns
* Quality commit history
* Ability to use libraries
* Ability to create basic model and retrieve information from the databse

## Implementation and Conceptual Questions

Please answer this questions in a Markdown file and commit it to the repo.

Plase use english for the answers and you can use drawings for a better description (even pictures of by-hand drawings)

1. How were you debugging this mini-project? Which tools?
    * I'm debugging the project using symfony profiler (YOURDOMAIN/_profiler),
    there you can see a broadside of your api calls, logs, issues, database querys and so on.

2. How were you testing the mini-project?
    * The api itself can be tested using the swagger ui, you have access to all REST apicalls within this project.

3. Imagine this mini-project needs microservices with one single database, how would you draft an architecture?
    * I would point my Route53 domain to an Application Load Balancer (AWS). Behind the load balancer I set up Docker containers (Aws ECS) and each container resolves nginx + php + node, the application run inside thse containars but the all are connected to one escalable rds instance. Which can be improved by demand.
    ![Scheme](/docimages/diagram.png)

4. How would your solution differ when all over the sudden instead of saving to a Database you would have to call another external API to store and receive the commits.
    * to store I would setup symfony messenger. With Symfony Messenger I would create a queue with the results from the GIThub API, then I would create a consumer to trigger all those POST requests. Doing that so, it would be easier to handle lack of disponibility at the external API side.
    * to receive the data I would just create one service with guzzle client and forward this api call.

## Bonus Points
While not required any of the following will add some major bonus points to your submission:

* Setup an asset pipeline with Gulp, Grunt, etc.
* Use Angular
* Use Composer
* Use Docker
* Create a set of provisioning scripts with Puppet, Chef, Ansible, etc...
