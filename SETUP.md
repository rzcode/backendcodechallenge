## DEPENDENCIES

 - You must have docker and docker composer installed if you want to set up automatically the environment for development.

#DEV Environment

```bash
$ docker-compose up
```

*Note: This task should take no longer than 5 minutes*
*Note: This docker file is not 100% done.

### Installation(inside the php container)

```bash
$ composer install
$ ./bin/console doctrine:database:create
$ ./bin/console doctrine:migrations:migrate
$ npm install
$ npm run build
```

## into your etc/hosts (in you machine)
put a new entry like with the same domain you've chose on you .env.local($NGINX_HOST)
```
127.0.0.1 backendcodechallenge.local
```

## Application

You can access the application in the root url
```
Ex: backendcodechallenge.loca:8080
```
The api Swagger can be found here
```
Ex: backendcodechallenge.loca:8080/api/docs
```
